<?
Class CadastroUnidade extends CadastroBloco{

    protected $id;

    function __construct(){

    }


    function getUnidades($id = null){
        $qry = 'SELECT 
        uni.id, 
        uni.numeroUnidade, 
        uni.metragem,
        uni.vagasGaragem, 
        bl.nomeBloco, 
        cond.nomeCondominio, 
        uni.idBloco, 
        uni.idCondominio
        FROM
        fv_unidade uni
        LEFT JOIN fv_bloco bl ON uni.idBloco = bl.id
        LEFT JOIN fv_condominios cond ON uni.idCondominio = cond.id';
        if($id){
            $qry .= ' WHERE uni.id=' .$id;
            $unique = true;
        }
        return $this->listarData($qry,$unique, 3);
    }

    function setUnidades($dados){
    
        $values = '';
        $sql = 'INSERT INTO fv_unidade (';

        foreach($dados as $ch=>$value){
          
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql, ', ');
        $sql .=') VALUES ('.rtrim($values,', ') . ');';

        return $this->insertData($sql);
    }

    function editUnidades($dados){
        $sql = 'UPDATE fv_unidade SET';
        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .=" `".$ch."` = '".$value."', ";
            }
        }
        
            $sql = rtrim($sql,', ');
            $sql .=' WHERE ID='.$dados['editar'];
    
            return $this->insertUpdate($sql);
    }

    function deletaUnidades($id){
        $qry = 'DELETE FROM  fv_unidade WHERE id='.$id;
        return $this->Delete($qry);
    }

    function getUnidadeFromBloco($id){
        $qry = 'SELECT id, numeroUnidade FROM fv_unidade WHERE idBloco = '.$id;
        return $this->listarData($qry);
    }
}




?>