<?
Class Consultas extends Dao{
    function getCondMoradores($id = null){
        $qry = 'SELECT
        cond.nomeCondominio,
        COUNT(cad.id) AS Total
        FROM 
        fv_cadastro cad
        LEFT JOIN fv_condominios cond ON cad.idCondominio = cond.id
        GROUP BY cad.idCondominio';
        if($id){
            $qry .= ' WHERE cond.id=' .$id;
        }
        return $this->listarData($qry);
    }

    function getLastAdm($id = null){
        $qry = 'SELECT
        nomeAdm
        FROM
        fv_administradora
        ORDER BY dataCadastro DESC
        LIMIT 5';
        if($id){
            $qry .= ' WHERE cond.id=' .$id;
        }
        return $this->listarData($qry);
    }
}
?>