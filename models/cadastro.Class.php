<?
Class Cadastro extends CadastroUnidade{
    // public $nome;
    // public $cpf;
    // public $email;
    // public $fone;
    //protected $dados = array();
    protected $id;

    function __construct(){

    }

    function getClientes($id = null){
        $qry = 'SELECT * FROM vw_consulta_cadastro';
        $contaTermos =count($this->busca);

        $isNull = false;

        if($contaTermos > 0){
            
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                        break;
                        default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }

            }
            $qry = rtrim($qry, ' AND');
        }
        if($id){
            $qry .= ' WHERE id=' .$id;
            $unique = true;
        }
        return $this->listarData($qry,$unique);
    }


    function setCliente($dados){
    
        $values = '';
        $sql = 'INSERT INTO fv_cadastro (';

        foreach($dados as $ch=>$value){
          
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql, ', ');
        $sql .=') VALUES ('.rtrim($values,', ') . ');';

        return $this->insertData($sql);
    }

    function editCliente($dados){
        $sql = 'UPDATE fv_cadastro SET';
        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .=" `".$ch."` = '".$value."', ";
            }
        }
        
            $sql = rtrim($sql,', ');
            $sql .=' WHERE id='.$dados['editar'];
    
            return $this->insertUpdate($sql);
    }

    function deletaCliente($id){
        $qry = 'DELETE FROM  fv_cadastro WHERE id='.$id;
        return $this->Delete($qry);
    }

}

?>