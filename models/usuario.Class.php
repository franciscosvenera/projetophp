<?
Class Usuarios extends Dao{

    function __construct(){
        
    }

    function getUsuarios($id){
        $qry = 'SELECT * FROM fv_usuarios';
        if($id){
            $qry .= ' WHERE id ='.$id;
        }
        return $this->listarData($qry);
    }

    function addUsuario($dados){
        $values ='';
          $qry = 'INSERT INTO fv_usuarios (';
          foreach($dados as $ch=> $value){
              $qry .='`'.$ch.'`, ';
              $values .= "'".$value."', ";
          }
          $qry = rtrim($qry,', ');
          $qry .= ') VALUES ('.rtrim($values,', ').')';
          return $this->insertData($qry);
      }
      function editaUsuario($dados){
       
        $qry = "UPDATE  fv_usuarios SET";
        foreach($dados as $ch=> $value){
            if($ch !='editar'){
                $qry .="`".$ch."` = '".$value."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=' WHERE id='.$dados['editar'];
        return $this->insertUpdate($qry);
      }
      function deletaUsuario($id){
        $qry = 'DELETE FROM fv_usuarios WHERE id ='.$id;
        return $this->Delete($qry);
      }
}
?>