<?
Class Conselho extends CadastroCondominio{
    // public $nome;
    // public $cpf;
    // public $email;
    // public $fone;
    //protected $dados = array();
    protected $id;

    function __construct(){

    }

    function getConselhos($id = null){
        $qry = 'SELECT * FROM fv_conselho';
        if($id){
            $qry .= ' WHERE id=' .$id;
        }
        return $this->listarData($qry);
    }

    function setConselhos($dados){
    
        $values = '';
        $sql = 'INSERT INTO fv_conselho (';

        foreach($dados as $ch=>$value){
          
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql, ', ');
        $sql .=') VALUES ('.rtrim($values,', ') . ');';

        return $this->insertData($sql);
    }

    function editConselhos($dados){
        $sql = 'UPDATE fv_conselho SET';
        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .=" `".$ch."` = '".$value."', ";
            }
        }
        
            $sql = rtrim($sql,', ');
            $sql .=' WHERE ID='.$dados['editar'];
    
            return $this->insertUpdate($sql);
    }

    function deletaConselhos($id){
        $qry = 'DELETE FROM  fv_conselho WHERE id='.$id;
        return $this->Delete($qry);
    }

}

?>