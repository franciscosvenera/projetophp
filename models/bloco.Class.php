<?
Class CadastroBloco extends CadastroCondominio{

    protected $dados = array();
    protected $id;

    function __construct(){

    }

    function getBlocos($id = null){
        $qry = 'SELECT * FROM vw_consulta_bloco';
        if($id){
            $qry .= ' WHERE id = ' .$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique, 3);
    }

    function getBlocosFromCond($cond){
        $qry= 'SELECT id, nomeBloco FROM fv_bloco WHERE idCondominio = '.$cond;
        return $this->listarData($qry);
    }

    function setBlocos($dados){
    
        $values = '';
        $sql = 'INSERT INTO fv_bloco (';

        foreach($dados as $ch=>$value){
          
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql, ', ');
        $sql .=') VALUES ('.rtrim($values,', ') . ');';

        return $this->insertData($sql);
    }


    function editBlocos($dados){
        $sql = 'UPDATE fv_bloco SET';
        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .=" `".$ch."` = '".$value."', ";
            }
        }
        
            $sql = rtrim($sql,', ');
            $sql .=' WHERE ID='.$dados['editar'];
    
            return $this->insertUpdate($sql);
    }

    function deletaBlocos($id){
        $qry = 'DELETE FROM  fv_bloco WHERE id='.$id;
        return $this->Delete($qry);
    }

}

?>