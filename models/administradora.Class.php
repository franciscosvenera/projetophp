<?
Class CadastroAdministradora extends Dao{

    protected $dados = array();
    protected $id;

    function __construct(){

    }

    function getAdministradora($id = null){
        $qry = 'SELECT * FROM fv_administradora';
        if($id){
            $qry .= ' WHERE id=' .$id;
        }
        return $this->listarData($qry);
    }

    function setAdministradora($dados){
    
        $values = '';
        $sql = 'INSERT INTO fv_administradora (';

        foreach($dados as $ch=>$value){
          
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql, ', ');
        $sql .=') VALUES ('.rtrim($values,', ') . ');';

        return $this->insertData($sql);
    }


    function editAdministradora($dados){
        $sql = 'UPDATE fv_administradora SET';
        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .=" `".$ch."` = '".$value."', ";
            }
        }
        
            $sql = rtrim($sql,', ');
            $sql .=' WHERE ID='.$dados['editar'];
    
            return $this->insertUpdate($sql);
    }

    function deletaAdministradora($id){
        $qry = 'DELETE FROM  fv_administradora WHERE id='.$id;
        return $this->Delete($qry);
    }


}

?>