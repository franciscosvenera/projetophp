<?
Class CadastroCondominio extends CadastroAdministradora{

    //protected $dados = array();
    protected $id;

    function __construct(){

    }

    function getCondominios($id = null){
        $qry = 'SELECT
        cond.id, cond.nomeCondominio, cond.qtde, cond.logradouro, cond.numero, cond.bairro, cond.cidade, cond.uf, cond.cep, con.nome, adm.nomeAdm, cond.sindico, cond.idAdmin
        FROM
        fv_condominios cond
        LEFT JOIN fv_conselho con ON cond.sindico = con.id
        LEFT JOIN fv_administradora adm ON cond.idAdmin = adm.id ';
        if($id){
            $qry .= ' WHERE cond.id=' .$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique, 3);
    }

    function setCondominio($dados){
    
        $values = '';
        $sql = 'INSERT INTO fv_condominios (';

        foreach($dados as $ch=>$value){
          
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql, ', ');
        $sql .=') VALUES ('.rtrim($values,', ') . ');';

        return $this->insertData($sql);
    }


    function editCondominio($dados){
        $sql = 'UPDATE fv_condominios SET';
        foreach ($dados as $ch => $value) {
            if($ch != 'editar'){
                $sql .=" `".$ch."` = '".$value."', ";
            }
        }
        
            $sql = rtrim($sql,', ');
            $sql .=' WHERE ID='.$dados['editar'];
    
            return $this->insertUpdate($sql);
    }

    function deletaCondominio($id){
        $qry = 'DELETE FROM  fv_condominios WHERE id='.$id;
        return $this->Delete($qry);
    }


}

?>