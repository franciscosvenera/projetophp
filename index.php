<?include "uteis.php";

$user = new Usuario();
if(!$user->acesso()){
    header("Location: login.php");
}
if(($_GET['page'] == 'logout')){
    if($user->logout()){
        header('Location: '.$url_site.'login.php');
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=$url_site?>css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Dashboard Condomínios</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.php">
            Condominios
        </a>
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" id="dropInqu" data-toggle="dropdown" aria-expanded="false">
                Cadastros
            </button>
            <div class="dropdown-menu" aria-labelledby="dropInqu">
                <a class="dropdown-item" href="<?=$url_site?>cadastro">Clientes</a>
                <a class="dropdown-item" href="<?=$url_site?>condominio">Condomínios</a>
                <a class="dropdown-item" href="<?=$url_site?>bloco">Blocos</a>
                <a class="dropdown-item" href="<?=$url_site?>unidade">Unidades</a>
                <a class="dropdown-item" href="<?=$url_site?>conselho">Conselho fiscal</a>
                <a class="dropdown-item" href="<?=$url_site?>administradora">Administradora</a>
                <a class="dropdown-item" href="<?=$url_site?>cadastroUsuario">Usuário</a>
            </div>
        </div>
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" id="dropInquilino" data-toggle="dropdown" aria-expanded="false">
                Listagens
            </button>
            <div class="dropdown-menu" aria-labelledby="dropInquilino">
                <a class="dropdown-item" href="<?=$url_site?>clientes">Lista Clientes</a>
                <a class="dropdown-item" href="<?=$url_site?>listaCondominios">Lista Condomínios</a>
                <a class="dropdown-item" href="<?=$url_site?>listaBlocos">Lista Blocos</a>
                <a class="dropdown-item" href="<?=$url_site?>listaUnidades">Lista Unidades</a>
                <a class="dropdown-item" href="<?=$url_site?>listaConselho">Lista Conselho</a>
                <a class="dropdown-item" href="<?=$url_site?>listaAdministradoras">Lista Administradoras</a>
                <a class="dropdown-item" href="<?=$url_site?>listaUsuarios">Lista de usuários</a>
            </div>
        </div>
        <a href="<?=$url_site?>logout"  class="nav-link text-dark"><i class="bi bi-box-arrow-right" action="logout"></i></a>
        <a href="<?=$url_site?>configuracoes"  class="nav-link text-dark"><i class="bi bi-gear-fill"></i></a>
        <?
        $nome = explode(' ', $_SESSION['USUARIO']['nome'])
        ?>
        <small>Olá <?=$nome[0];?>, seja bem vindo!</small>
    </nav>
    <main class="container">



    <?
            switch ($_GET['page']) {
                case '':
                case 'inicio':
                    require "controllers/inicio.php";
                    require "views/inicio.php";
                    break;
                default:
                require 'controllers/'.$_GET['page'].'.php';
                    require 'views/'.$_GET['page'].'.php';
                    break;
                
            };
    ?>

</main>
<footer class='col-12' style="bottom: 0; position: fixed;">
<div class="text-center">
            <span>&copy;Cadastro de clientes</span>
            <span>Suporte</span>
            <a href="#"><i class="icofont-whatsapp"></i>47 9999-9999</a>
            <i class="icofont-lamp-light icofont-3x"></i>
        </div>
</footer>
<script>var url_site = '<?=$url_site?>';</script>
<script src="<?=$url_site?>js/jquery-3.6.0.min.js"></script>
<script src="<?=$url_site?>js/bootstrap.min.js"></script>
<script src="<?=$url_site?>js/app.js"></script>
<script src="<?$url_site?>js/jquery.mask.min.js"></script>
</body>
</html>




