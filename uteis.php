
<?

$localDir = 'projetophp/';
$base_url = "http://".$_SERVER['HTTP_HOST'].'/';

$url_site = $base_url.$localDir;

$fullPath = $_SERVER['DOCUMENT_ROOT'].'/';
$fullPath .= $localDir;

$includes = $fullPath.'includes/';
$models = $fullPath.'models/';
$controllers = $fullPath.'controllers/';
$views = $fullPath.'views/';


session_start();
error_reporting('E_FATAL | E_PARSE');
require $models."conectDB.class.php";
require $models."dao.Class.php";
include $models."administradora.Class.php";
require $models."control.Class.php";
include $models."condominio.Class.php";
include $models."conselho.Class.php";
include $models."bloco.Class.php";
include $models."unidade.Class.php";
include $models."cadastro.Class.php";
include $models."consultas.Class.php";
require $models."restrito.Class.php";
require $models."usuario.Class.php";

define('DEBUG',true);
function legivel($var,$width = '250',$height = '400') {
    if (DEBUG){
        echo "<pre>";
        if(is_array($var)) {
            print_r($var);
        } else {
            print($var);
        }
        echo "</pre>";
    }
}

function trataUrl($params = array()){
    $url = (isset($_GET['b'])) ? 'busca/' : '';
    foreach($params as $value){
        $url .= $value.'/';
    }
    return $url;
}

$nav = array(
    //'inicio' => 'Dashboard',
    'Cadastro' => array(
        'cadastro' => 'Cadastro',
        'condominio' => 'Condomínios',
        'bloco' => 'Cadastro de blocos',
        'unidade' => 'Cadatro de unidades',
        'conselho' => 'Cadastro do conselho fiscal',
        'cadastroAdministradora' => 'Cadastro de administradora'
    ),
        'Listas' => array(
        'clientes' => 'Listar Clientes',
        'listaCondominios' => 'Lista de condomínios',
        'listaBlocos' => 'Lista de blocos',
        'listaUnidades' =>  'Lista de unidades',
        'listaAdministradoras' =>  'Lista de administradoras'
    )
);

function dateFormat($d, $tipo = true){
    if(!$d){
        return '';
    }
    if ($tipo){
        $hora = explode(' ',$d);
        $data = explode('-',$hora[0]);
        return $data[2].'/'.$data[1].'/'.$data[0].' '.$hora[1];
    } else{
        $hora = explode(' ',$d);
        $data = explode('/',$hora[0]);
        return $data[2].'-'.$data[1].'-'.$data[0].' '.$hora[1];
    }

}
function antiinject($var,$quotes=ENT_NOQUOTES,$keeptags=false) {
    //ENT_QUOTES, ENT_NOQUOTES, ENT_COMPAT;
    
    
    if(!is_array($var)){
        $var = stripslashes($var);
        $var = html_entity_decode($var,$quotes,'utf-8');
        if(!$keeptags) {
            $var = strip_tags($var);
        }
        $var = trim($var);
        //$var = utf8_decode($var);
        /**/
        $var = htmlentities($var,$quotes);
        if($keeptags) {
            $var = str_replace('&lt;','<',$var);
            $var = str_replace('&gt;','>',$var);
        }
        /**/
        $var = addslashes($var);
    } else {
        foreach($var as $k=>$ar){
            $var[$k] = antiinject($ar);
        }
    }
    return $var;
}

$estados = array( // Graças ao google os programadores preguiçosos não precisam estudar geografia, http://snipplr.com/view/27496/array-de-estados-brasileiros/
    "AC" => "Acre", 
    "AL" => "Alagoas", 
    "AM" => "Amazonas", 
    "AP" => "Amapá",
    "BA" => "Bahia",
    "CE" => "Ceará",
    "DF" => "Distrito Federal",
    "ES" => "Espírito Santo",
    "GO" => "Goiás",
    "MA" => "Maranhão",
    "MT" => "Mato Grosso",
    "MS" => "Mato Grosso do Sul",
    "MG" => "Minas Gerais",
    "PA" => "Pará",
    "PB" => "Paraíba",
    "PR" => "Paraná",
    "PE" => "Pernambuco",
    "PI" => "Piauí",
    "RJ" => "Rio de Janeiro",
    "RN" => "Rio Grande do Norte",
    "RO" => "Rondônia",
    "RS" => "Rio Grande do Sul",
    "RR" => "Roraima",
    "SC" => "Santa Catarina",
    "SE" => "Sergipe",
    "SP" => "São Paulo",
    "TO" => "Tocantins"
);

$funcoes = array(
    "Síndico",
    "Sub-síndico",
    "Conselheiro",
    "Contador"
)


?>