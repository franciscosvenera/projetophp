<?
require "../uteis.php";

$blocos = new CadastroBloco();
$dados = $blocos->getBlocosFromCond($_REQUEST['id']);

if(!empty($dados)){
    $result = array(
        "status" => 'success',
        "resultSet" => $dados['resultSet']
    );
    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "Não pode ser inserido"
    );

    echo json_encode($result);
}


?>