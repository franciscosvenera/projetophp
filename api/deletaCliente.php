<?

use LDAP\Result;

require "../uteis.php";
// require "../class/control.Class.php";
// require "../class/cadastro.Class.php";

$cliente = new Cadastro();
if($cliente->deletaCliente($_POST['id'])){


    $totalRegistros = count($_SESSION['cadastro']);

    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Parabéns, seu registro foi deletado"
    );

    echo json_encode($result);
}else{
    $result = array(
        'status' => 'danger',
        'msg' => 'O registro não pode ser deletado'
    );
    echo json_encode($result);
}
?>