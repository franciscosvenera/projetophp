<?

require "../uteis.php";
// require "../class/bloco.Class.php";

$bloco = new CadastroBloco();
if($bloco->deletaBlocos($_POST['id'])){


    $totalRegistros = count($_SESSION['cadastro']);

    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Parabéns, seu bloco foi deletado"
    );

    echo json_encode($result);
}else{
    $result = array(
        'status' => 'danger',
        'msg' => 'O bloco não pode ser deletado'
    );
    echo json_encode($result);
}
?>