<?

require "../uteis.php";

$administradora = new CadastroAdministradora();
$result = $administradora->deletaAdministradora($_POST['id']);
if($result){


    $totalRegistros = $administradora->getAdministradora()['totalResults'];

    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Parabéns, sua administradora foi deletada"
    );

    echo json_encode($result);
}else{
    $result = array(
        'status' => 'danger',
        'msg' => 'A administradora não pode ser deletada'
    );
    echo json_encode($result);
}
?>