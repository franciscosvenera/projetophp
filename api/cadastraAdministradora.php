<?
require "../uteis.php";

$administradora = new CadastroAdministradora();
if ($administradora->setAdministradora($_POST)){
    $result = array(
        "status" => 'success',
        "msg" => "Administradora inserida com sucesso"
    );

    echo json_encode($result);
}else{
    $result = array(
        "status" => 'success',
        "msg" => "Registro não pode ser inserido"

    );

    echo json_encode($result);
}
?>