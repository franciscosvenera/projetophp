<?
require "../uteis.php";

$administradora = new CadastroAdministradora();
if ($administradora->editAdministradora($_POST)){
    $result = array(
        "status" => 'success',
        "msg" => "Administradora atualizada com sucesso."
    );

    echo json_encode($result);
};
?>