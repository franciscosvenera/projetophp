<?

require "../uteis.php";
// require "../class/condominio.Class.php";

$condominio = new CadastroCondominio();
$result = $condominio->deletaCondominio($_POST['id']);
if($result){


    $totalRegistros = $condominio->getCondominios()['totalResults'];

    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Parabéns, seu condomínio foi deletado"
    );

    echo json_encode($result);
}else{
    $result = array(
        'status' => 'danger',
        'msg' => 'O condominio não pode ser deletado'
    );
    echo json_encode($result);
}
?>