<?

require "../uteis.php";

$usuario = new Usuarios();
if($bloco->deletaUsuarios($_POST['id'])){


    $totalRegistros = count($_SESSION['cadastro']);

    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Parabéns, seu usuario foi deletado"
    );

    echo json_encode($result);
}else{
    $result = array(
        'status' => 'danger',
        'msg' => 'O usuário não pode ser deletado'
    );
    echo json_encode($result);
}
?>