<?
require "../uteis.php";


$unidades = new CadastroUnidade();
$dados = $unidades->getUnidadeFromBloco($_REQUEST['id']);

if(!empty($dados)){
    $result = array(
        "status" => 'success',
        "resultSet" => $dados['resultSet']
    );

}else{
    $result = array(
        "status" => 'danger',
        "msg" => "Não pode ser inserido"
    );

}

echo json_encode($result);

?>