<?

require "../uteis.php";
// require "../class/unidade.Class.php";

$unidade = new CadastroUnidade();
if($unidade->deletaUnidades($_POST['id'])){


    $totalRegistros = count($_SESSION['cadastro']);

    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Parabéns, a unidade foi excluída com sucesso"
    );

    echo json_encode($result);
}else{
    $result = array(
        'status' => 'danger',
        'msg' => 'A unidade não pode ser deletada'
    );
    echo json_encode($result);
}
?>