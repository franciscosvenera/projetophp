$(function(){
    $('#cadastroCliente').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = url_site+'api/editacliente.php';
            urlRedir = url_site+'clientes';
        }else{
            url = url_site+'api/cadastraCliente.php'
            urlRedir = url_site+'clientes'
        }
        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),

            success : function(data){
                if(data == 'success'){
                   myAlert(data.status, data.msg, 'main', urlRedir);
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })

        return false;
    })
    
    $('#cadastroCondominio').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = url_site+'api/editaCondominio.php';
            urlRedir = url_site+'listaCondominios';
        }else{
            url = url_site+'api/cadastraCondominio.php'
            urlRedir = url_site+'listaCondominios'
        }
        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),

            success : function(data){
                if(data == 'success'){
                   myAlert(data.status, data.msg, 'main', urlRedir);
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })

        return false;
    })

    $('.fromCondominio').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listBlocos.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado},
            success : function(data){
                selectPopulation('.fromBloco',data.resultSet, 'nomeBloco');
            }
        })
    })

    function selectPopulation(seletor, dados, fild){
        estrutura = '<option value="">Selecione...<option/>';

        for (let i=0; i < dados.length; i++) {

            estrutura += '<option value="'+dados[i].id+'">'+dados[i][fild]+'</option>';
        }
        $(seletor).html(estrutura)
    }

    $('#cadastroBloco').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = url_site+'api/editaBloco.php';
            urlRedir = url_site+'listaBlocos';
        }else{
            url = url_site+'api/cadastraBloco.php'
            urlRedir = url_site+'listaBlocos'
        }
        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),

            success : function(data){
                if(data == 'success'){
                   myAlert(data.status, data.msg, 'main', urlRedir);
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })

        return false;
    })

    $('#cadastroUnidade').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = url_site+'api/editaUnidade.php';
            urlRedir = url_site+'listaUnidades';
        }else{
            url = url_site+'api/cadastraUnidade.php'
            urlRedir = url_site+'listaUnidades'
        }
        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),

            success : function(data){
                if(data == 'success'){
                   myAlert(data.status, data.msg, 'main', urlRedir);
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })

        return false;
    })

    //chamar unidade após selecionar blocos

    $('.fromBloco').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listUnidades.php',
            dataType: 'json',
            type:'POST',
            data: {id: selecionado},
            success: function(data){
                selectPopulation('.fromUnidade', data.resultSet, 'numeroUnidade');
            }
        })
    })

    $('#cadastroConselho').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = url_site+'api/editaConselho.php';
            urlRedir = url_site+'listaConselho';
        }else{
            url = url_site+'api/cadastraConselho.php'
            urlRedir = url_site+'listaConselho'
        }
        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),

            success : function(data){
                if(data == 'success'){
                   myAlert(data.status, data.msg, 'main', urlRedir);
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })

        return false;
    })

    $('#cadastroAdministradora').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = url_site+'api/editaAdministradora.php';
            urlRedir = url_site+'listaAdministradoras';
        }else{
            url = url_site+'api/cadastraAdministradora.php'
            urlRedir = url_site+'listaAdministradoras'
        }
        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),

            success : function(data){
                if(data == 'success'){
                   myAlert(data.status, data.msg, 'main', urlRedir);
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })

        return false;
    })

    $('#cadastroUsuario').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = url_site+'api/editaUsuario.php';
            urlRedir = url_site+'listaUsuarios';
        }else{
            url = url_site+'api/cadastraUsuario.php'
            urlRedir = url_site+'listaUsuarios'
        }
        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),

            success : function(data){
                if(data == 'success'){
                   myAlert(data.status, data.msg, 'main', urlRedir);
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })

        return false;
    })

    $('#listaClientes').on('click','.removerCliente', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaCliente.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success: function(data){
                if (data.status == 'success'){
                    
                    myAlert(data.status, data.msg, 'main',url_site+'clientes');    
                }else{
                    myAlert(data.status, data.msg, 'main');
                }  
            }
        })
        return false;
    })
    
    $('#listaCondominios').on('click','.removerCondominio', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaCondominio.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success: function(data){
                if (data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',url_site+'listaCondominios');
                }else{
                    myAlert(data.status, data.msg, 'main');
                }  
            }
        })
        return false;
    })

    $('#listaBlocos').on('click','.removerBloco', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaBloco.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success: function(data){
                if (data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',url_site+'listaBlocos');    
                }else{
                    myAlert(data.status, data.msg, 'main');
                }  
            }
        })
        return false;
    })

    $('#listaUnidades').on('click','.removerUnidade', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaUnidade.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success: function(data){
                if (data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',url_site+'listaUnidades');    
                }else{
                    myAlert(data.status, data.msg, 'main');
                }  
            }
        })
        return false;
    })

    $('#listaConselho').on('click','.removerConselho', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaConselho.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success: function(data){
                if (data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',url_site+'listaConselho');    
                }else{
                    myAlert(data.status, data.msg, 'main');
                }  
            }
        })
        return false;
    })

    $('#listaAdministradoras').on('click','.removerAdministradora', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaAdministradora.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success: function(data){
                if (data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',url_site+'listaAdministradora');    
                }else{
                    myAlert(data.status, data.msg, 'main');
                }  
            }
        })
        return false;
    })

    $('#listaUsuarios').on('click','.removerUsuario', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaUsuario.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success: function(data){
                if (data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',url_site+'listaUsuarios');    
                }else{
                    myAlert(data.status, data.msg, 'main');
                }  
            }
        })
        return false;
    })

    function myAlert(tipo, mensagem, pai, url){
        url = (url == undefined) ? url = '' : url = url;
        componente = '<div class="alert alert-'+tipo+'" role="alert">'+mensagem+'</div>';
        $(pai).prepend(componente);
        
        setTimeout(function(){
            $(pai).find('div.alert').remove();
            if(tipo == 'success' && url){
                setTimeout(function(){
                    window.location.href = url;
                },1000)
            }
        },2000)
    }

});

//controlador do filtro

$('#filtro').submit(function(){
    var pagina = $('input[name="page"]').val();
    var termo1 = $('.termo1').val();
    var termo2 = $('.termo2').val();

    termo1 = (termo1) ? termo1+'/' : '';
    termo2 = (termo2) ? termo2+'/' : '';

    window.location.href = url_site+pagina+'/busca/'+termo1+termo2

    return false;
})


function verificaCampos(){
    var termo1 = $('.termo1').val();
    var termo2 = $('.termo2').val();
    if(termo1 || termo2){
        $('button[type="submit"]').prop('disabled',false);
    }else{
        $('button[type="submit"]').prop('disabled',true);
    }
}

$('.termo1, .termo2').keyup(function(){
    verificaCampos();
})
$('.termo1, .termo2').focusout(function(){
    verificaCampos();
})
$('.termo1, .termo2').change(function(){
    verificaCampos();
})


//criar mascara

$('input[name="cpf"]').mask('999.999.999-99', {reverse:true});
$('input[name="telefone"]').mask('(99) 99999-9999');
$('.cnpj').mask('00.000.000/0000-00', {reverse: true});
$('.cep').mask('00000-000');

function myAlert(tipo, mensagem, pai, url){
    url = (url == undefined) ? url = '' : url = url;
    componente = '<div class="alert alert-'+tipo+'" role="alert">'+mensagem+'</div>';

    $(pai).prepend(componente);

    setTimeout(function(){
        $(pai).find('div.alert').remove();
        //vai redirecionar?
        if(tipo == 'success' && url){
            setTimeout(function(){
                window.location.href = url;
            },500);
        }
    },2000)

}