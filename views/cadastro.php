
<form action="" method="post" id="cadastroCliente">
    Nome:
    <input class="col col-6 mt-2 form-control" type="text" name="nome" value="<?=$cliente['nome']?>" required>
    CPF:
    <input data-mask="000.000.000-00" class="col col-6 mt-2 form-control" type="text" name="cpf" value="<?=$cliente['cpf']?>" required>
    E-mail:
    <input class="col col-6 mt-2 form-control" type="email" name="email" value="<?=$cliente['email']?>" required>
    Fone:
    <input class="col col-6 mt-2 form-control" type="text" name="fone" value="<?=$cliente['fone']?>">

    Condomínio:
    <select name="idCondominio" class="form-control fromCondominio">
        
        <?foreach ($resultCondominio as $ch => $value) {?>
            <option value="<?=$value['id']?>"<?=($value['nomeCondominio'] == $cliente['nomeCondominio'] ? 'selected' : '')?>><?=$value['nomeCondominio']?></option>
        <?}?>
    </select>

    Bloco:
    <select name="idBloco" class="form-control fromBloco custom-select">
        <?
        if ($_GET ['id']){
            $blocos = $clientes->getBlocosFromCond($cliente['idCondominio']);
            foreach($blocos['resultSet'] as $bloco){
            
        ?>
        <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $cliente['idBloco'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
        <?} }?>    
    </select>
    
    Unidade:
    <select name="idUnidade" class="form-control fromUnidade">
    <?
        if ($_GET ['id']){
            $unidades = $clientes->getUnidadeFromBloco($cliente['idBloco']);
            foreach($unidades['resultSet'] as $unidade){
            
        ?>
        <option value="<?=$unidade['id']?>"<?=($unidade['id'] == $cliente['idUnidade'] ? 'selected' : '')?>><?=$unidade['numeroUnidade']?></option>
        <?} }?>   
    </select>


    <? if($_GET['id']){?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <?}?>
    <br>
    <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>
