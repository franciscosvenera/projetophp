<?
//require "class/cadastro.Class.php";
?>
<table class="col col-12 table table-striped mt-5" id="listaClientes">
<div class="row">
    <div class="col-12">

        <tr>
            <td colspan="5">
                <form class="form-inline" method="GET" id="filtro">
                    <input type="hidden" name="page" value="clientes">
                    <input class="form-control mr-sm-2 termo1" type="search" placeholder="Buscar por nome" aria-label="Search" name="b[nome]">
                    <div class="input-group-prepend">
                        <div class="input-group-text">por condomínio</div>
                    </div>
                    <select name="b[idCondominio]" class="custom-select termo2">
                        <option value="">...</option>
                        <?
                       
                        foreach ($listCondominio['resultSet'] as $condominios) {
                            echo '<option value="' . $condominios['id'] . '">' . $condominios['nomeCondominio'] . '</option>';
                        } ?>
                    </select>
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" disabled>Buscar</button>
                    <a class="btn btn-outline-success my-2 my-sm-0" href="<?=$url_site?>clientes">Limpar</a>
                </form>
            </td>
        </tr>

            <tr>
                <td>Nome</td>
                <td>Documento</td>
                <td>Email</td>
                <td>Telefone</td>
                <td>Condominio</td>
                <td>Bloco</td>
                <td>Unidade</td>
                <td>Data Cad.</td>
                <td>Data At.</td>
                <td><a href="<?=$url_site?>cadastro" class="btn btn-primary">Adicionar</a> </td>
            </tr>
            <?

            foreach ($result['resultSet'] as $chDados => $campos) {

                //foreach ($cliente->getClientes() as $chDados => $campos) { 

            ?>
                <tr data-id="<?= $campos['id'] ?>">
                    <td><?= $campos['nome'] ?></td>
                    <td><?= $campos['cpf'] ?></td>
                    <td><?= $campos['email'] ?></td>
                    <td><?= $campos['fone'] ?></td>
                    <td><?= $campos['nomeCondominio'] ?></td>
                    <td><?= $campos['nomeBloco'] ?></td>
                    <td><?= $campos['numeroUnidade'] ?></td>
                    <td><?= dateFormat($campos['dataCadastro']) ?></td>
                    <td><?= dateFormat($campos['dataUpdate']) ?></td>
                    <td>
                        <a href="<?=$url_site?>cadastro/id/<?= $campos['id'] ?>">Editar</a>
                        <a href="#" data-id="<?= $campos['id'] ?>" class="removerCliente">Remover</a>
                    </td>
                </tr>
            <? } ?>
            
        </table>
        <div class="row">
            <?=$paginacao?>
            <div class="totalRegistros col-sm-6">Total registros: <?=$totalRegistros?>
        </div>
    </div>
</div>

<?
if (!empty($_GET['deletar'])) {
    unset($_SESSION['cadastro'][$_GET['deletar']]);
    header("Location: index.php?page=clientes");
}
?>