<table class="col col-12 table table-striped mt-5" id="listaClientes">
    <tr>
        <td colspan="4">
            <form class="form-inline my-2 my-lg-0" action="index.php" method="GET">
                <input type="hidden" name="page" value="listaMoradores">
                <div class="input-group-prepend">
                    <div class="input-group-text">Busca por nome</div>
                </div>
                <input class="form-control mr-sm-2" type="search" placeholder="" aria-label="Search" name="b[nome]">

                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                <a class="btn btn-outline-success my-2 my-sm-0" href="index.php?page=listaMoradores">Limpar</a>
            </form>
        </td>
    </tr>
    <tr>
        <td>Nome</td>
        <td>Usuário</td>
        <td>Data Cadastro</td>
        <td align="center"><a href="<?=$url_site?>cadastroUsuario" class="btn btn-primary btn-sm">Adicionar</a></td>
    </tr>
    <? foreach ($result['resultSet'] as $dados) {?>
        <tr data-id="<?=$dados['id']?>">
            <td><?= $dados['nome'] ?></td>
            <td><?= $dados['usuario'] ?></td>
            <td><?=dateFormat($dados['data'])?></td>
            <td align="center">
                <a href="<?=$url_site?>cadastroUsuario/id/<?=$dados['id']?>">Editar</a>
                <a href="#" data-id="<?=$dados['id']?>" class="removerCliente">Remover</i></a>
            </td>
        </tr>
    <? } ?>

</table>
<div class="row">
    <?=$paginacao;?>
    <div class="totalRegistros col-sm-6">Total Registros <?=$totalRegistros?></div>
</div>