<?


?>
<form action="" method="post" id="cadastroBloco">
    Nome do bloco:
    <input class="col col-6 mt-2 form-control" type="text" name="nomeBloco" value="<?=$bloco['nomeBloco']?>" required>
    Andares:
    <input class="col col-6 mt-2 form-control" type="text" name="andares" value="<?=$bloco['andares']?>" required>
    Unidades por andar:
    <input class="col col-6 mt-2 form-control" type="text" name="unidadesAndar" value="<?=$bloco['unidadesAndar']?>" required>
    Condomínio:
    <select name="idCondominio" class="form-control">
        
        <?foreach ($resultCondominio as $ch => $value) {?>
            <option value="<?=$value['id']?>"<?=($value['nomeCondominio'] == $bloco['nomeCondominio'] ? 'selected' : '')?>><?=$value['nomeCondominio']?></option>
        <?}?>
    </select>

    <? if($_GET['id']){?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <?}?>
    <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
    
</form>