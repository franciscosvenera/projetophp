
<form action="" method="post" id="cadastroUnidade">
    Número da unidade:
    <input class="col col-6 mt-2 form-control" type="text" name="numeroUnidade" value="<?=$unidade['numeroUnidade']?>" required>
    Metragem:
    <input class="col col-6 mt-2 form-control" type="text" name="metragem" value="<?=$unidade['metragem']?>" required>
    Vagas de garagem por unidade:
    <input class="col col-6 mt-2 form-control" type="text" name="vagasGaragem" value="<?=$unidade['vagasGaragem']?>" required>
    
    Condomínio:
    <select name="idCondominio" class="form-control">
        
        <?foreach ($resultCondominio as $ch => $value) {?>
            <option value="<?=$value['id']?>"<?=($value['nomeCondominio'] == $bloco['nomeCondominio'] ? 'selected' : '')?>><?=$value['nomeCondominio']?></option>
            <?}?>
    </select>
        
    Bloco:
    <select name="idBloco" class="form-control fromBloco custom-select">
        <?
        if ($_GET ['id']){
            $blocos = $clientes->getBlocosFromCond($cliente['idCondominio']);
            foreach($blocos['resultSet'] as $bloco){
            
        ?>
        <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $cliente['idBloco'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
        <?} }?>    
    </select>

    <? if($_GET['id']){?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <?}?>
    <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>