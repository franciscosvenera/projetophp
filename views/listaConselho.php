<?
//require "class/conselho.Class.php";
?>

<table class="col col-6 table table-striped mt-5" id="listaConselho">
    <tr>
        <td>Nome</td>
        <td>Documento</td>
        <td>Email</td>
        <td>Telefone</td>
        <td>Função</td>
        <td>Condomínio</td>

        <td><a href="<?=$url_site?>conselho" class="btn btn-primary">Adicionar</a> </td>
    </tr>
    <? 
    
        foreach ($result['resultSet'] as $chDados => $campos) {
            
    ?>
    <tr data-id="<?=$chDados?>">
            <td><?= $campos['nome'] ?></td>
            <td><?= $campos['cpf'] ?></td>
            <td><?= $campos['email'] ?></td>
            <td><?= $campos['fone'] ?></td>
            <td><?= $campos['funcao'] ?></td>
            <td><?= $campos['idCondominio'] ?></td>
            <td>
                <a href="?<?=$url_site?>conselho/id/<?=$campos['id']?>">Editar</a>
                <a href="#" data-id="<?=$campos['id']?>" class="removerConselho">Remover</a>
            </td>
    </tr>
<? } ?>
<tr>
    <td colspan="3"> &nbsp;</td>
    <td colspan="12" class="totalRegistros">Total de registros: <?=$totalRegistros?></td>
</tr>
</table>

<?
    if(!empty($_GET['deletar'])){
        unset($_SESSION['cadastro'][$_GET['deletar']]);
        header("Location: index.php?page=listaConselho");
    }
?>