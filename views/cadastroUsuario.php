<form action="" method="post" id="cadastroUsuario">
    <div class="form-row">

        <div class="col-sm-12 form-group">
            <label for="nome">Nome</label>
            <input class="form-control" type="text" name="nome" value="<?=$popular['nome']?>" required>
        </div>
        
        <div class="col-sm-3 form-group">
            <label for="usuario">Usuário</label>
            <input class="form-control" type="text" name="usuario" value="<?=$popular['usuario']?>" required>
        </div>

        <div class="col-sm-3 form-group">
            <label for="senha">Informar senha</label>
            <input class="form-control" type="password" name="senha" value="<?=$popular['senha']?>" required>
        </div>

        <!-- <div class="col-sm-3 form-group">
            <label for="senha">Confirmar senha</label>
            <input class="form-control" type="password" name="chsenha" required>
        </div> -->
        
        <? if($_GET['id']){?>
            <input type="hidden" name="editar" value="<?=$_GET['id']?>">
        <? } ?>
        <div class="col-sm-12">
            <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
        </div>
    </div>
</form>

