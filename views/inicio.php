<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Dashboard Condomínios</title>
</head>

<body>
    <br>
    <main class="container">
        <div class="row col-sm-12">
            <div class="card col-sm-6">
                <div class="card-body">
                <h5 class="card-title">Moradores por condomínio</h5>
                    <?$moradores = new Consultas();
                    $morador = $moradores->getCondMoradores();
                    foreach ($morador['resultSet'] as $value){?>
                    <ul class="list-group col-sm-12">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <?=$value['nomeCondominio']?>
                                <span class=""><?=$value['Total']?></span>
                            </li>
                        </ul>
                    <?}?>
                </div>
                
            </div>
            <div class="card col-sm-6">
                <div class="card-body">
                    <h5 class="card-title">Últimas 5 administradoras</h5>
                    <?
                    $condominios = new Consultas();
                    $condominio = $condominios->getLastAdm();
                    foreach ($condominio['resultSet'] as $value) {
                    ?>
                        <ul class="list-group col-sm-12">
                            <li class="list-group-item"><?=$value['nomeAdm']?></li>
                        </ul>
                    <?}?>
                </div>
            </div>
        </div>
        <br>
        <div class="row col-sm-12 text-center card-group">
            <div class="card">
                <div class="card-body">
                <?$condominio = new CadastroCondominio();
                $result = $condominio->getCondominios();?>
                <h5 class="totalRegistros col-sm-6"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></h5>
                    <p class="card-text">Condomínios cadastrados.</p>
                </div>
                <div class="card-footer">
                    <a href="?page=listaCondominios">Ver condomínios<small class="text-muted"></small></a>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                <?$condominio = new CadastroCondominio();
                $result = $condominio->getCondominios();?>
                <h5 class="totalRegistros col-sm-6"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></h5>
                    <p class="card-text">Condomínios cadastrados.</p>
                </div>
                <div class="card-footer">
                    <a href="?page=listaCondominios">Ver condomínios<small class="text-muted"></small></a>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                <?$cliente = new Cadastro();
                $result = $cliente->getClientes();?>
                <h5 class="totalRegistros col-sm-6"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></h5>
                    <p class="card-text">Moradores cadastrados.</p>
                </div>
                <div class="card-footer">
                    <a href="?page=clientes">Ver Moradores<small class="text-muted"></small></a>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <?$administradora = new CadastroAdministradora();
                    $result = $administradora->getAdministradora();?>
                    <h5 class="totalRegistros col-sm-6"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></h5>
                    <p class="card-text">Administradoras.</p>
                </div>
                <div class="card-footer">
                    <a href="?page=listaAdministradoras">Ver administradoras<small class="text-muted"></small></a>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <?$blocos = new CadastroBloco();
                    $result = $blocos->getBlocos();?>
                    <h5 class="totalRegistros col-sm-6"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></h5>
                    <p class="card-text">Blocos cadastrados.</p>
                </div>
                <div class="card-footer">
                    <a href="?page=listaBlocos">Ver blocos<small class="text-muted"></small></a>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <?$unidade = new CadastroUnidade();
                    $result = $unidade->getUnidades();?>
                    <h5 class="totalRegistros col-sm-6"><?= ($result['totalResults'] < 10) ? '0' . $result['totalResults'] : $result['totalResults'] ?></h5>
                    <p class="card-text">Unidades cadastradas.</p>
                </div>
                <div class="card-footer">
                    <a href="?page=listaUnidades">Ver unidades<small class="text-muted"></small></a>
                </div>
            </div>
        </div>
    </main>
</body>