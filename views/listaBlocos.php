<?
//require "class/bloco.Class.php";
?>

<table class="col col-6 table table-striped mt-5" id="listaBlocos">
    <tr>
        <td>Nome do bloco</td>
        <td>Andares</td>
        <td>Unidades por andar</td>
        <td>Condominio</td>
        <td>Data Cad.</td>
        <td>Data At.</td>
        <td><a href="<?=$url_site?>bloco" class="btn btn-primary">Adicionar</a> </td>
    </tr>
    <? 
        
        foreach ($result['resultSet'] as $chDados => $campos) { 
            
    ?>
    <tr data-id="<?=$campos['id']?>">
            <td><?= $campos['nomeBloco'] ?></td>
            <td><?= $campos['andares'] ?></td>
            <td><?= $campos['unidadesAndar'] ?></td>
            <td><?= $campos['nomeCondominio'] ?></td>
            <td><?= dateFormat($campos['dataCadastro'])?></td>
            <td><?= dateFormat($campos['dataUpdate'])?></td>
            <td>
                <a href="<?=$url_site?>bloco/id/<?=$campos['id']?>">Editar</a>
                <a href="#" data-id="<?=$campos['id']?>" class="removerBloco">Remover</a>
            </td>
    </tr>
<? } ?>
<tr>
    <td colspan="3"> &nbsp;</td>
    <td colspan="12" class="totalRegistros">Total de registros: <?=$totalRegistros ?></td>
</tr>
</table>
<div class="class col-sm-12">
    <?=$paginacao?>
</div>
<?
    if(!empty($_GET['deletar'])){
        unset($_SESSION['cadastro'][$_GET['deletar']]);
        header("Location: index.php?page=listaBlocos");
    }
?>