<?
//require "class/unidade.Class.php";
?>

<table class="col col-6 table table-striped mt-5" id="listaUnidades">
    <tr>
        <td>Número unidade</td>
        <td>Metragem</td>
        <td>Vagas garagem</td>
        <td>Bloco</td>
        <td>Condominio</td>
        <td><a href="<?=$url_site?>unidade" class="btn btn-primary">Adicionar</a> </td>
    </tr>
    <? 
        
        foreach ($result['resultSet'] as $chDados => $campos) { 
            
    ?>
    <tr data-id="<?=$chDados?>">
            <td><?= $campos['numeroUnidade'] ?></td>
            <td><?= $campos['metragem'] ?></td>
            <td><?= $campos['vagasGaragem'] ?></td>
            <td><?= $campos['nomeBloco'] ?></td>
            <td><?= $campos['nomeCondominio'] ?></td>
            <td>
            <a href="<?=$url_site?>bloco/id/<?=$campos['id']?>">Editar</a>
            <a href="#" data-id="<?=$campos['id']?>" class="removerUnidade">Remover</a>
            </td>
    </tr>
<? } ?>
<tr>
    <td colspan="3"> &nbsp;</td>
    <td colspan="12" class="totalRegistros">Total de registros: <?=$totalRegistros ?></td>
</tr>
</table>
<div class="class col-sm-12">
    <?=$paginacao?>
</div>
<?
    if(!empty($_GET['deletar'])){
        unset($_SESSION['cadastro'][$_GET['deletar']]);
        header("Location: index.php?page=listaUnidades");
    }
?>