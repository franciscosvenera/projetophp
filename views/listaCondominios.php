<?
//require "uteis.php";
?>

<table class="col col-6 table table-striped mt-5" id="listaCondominios">
    <tr>
        <td>Nome</td>
        <td>Qtde. unidades</td>
        <td>Logradouro</td>
        <td>Número</td>
        <td>Bairro</td>
        <td>Cidade</td>
        <td>Estado</td>
        <td>CEP</td>
        <td>Sindico</td>
        <td>Admin.</td>
        <td>Data Cad.</td>
        <td>Data At.</td>
        <td><a href="<?=$url_site?>condominio" class="btn btn-primary">Adicionar</a> </td>
    </tr>
    <? 
        
        foreach ($result['resultSet'] as $chDados => $campos) { 
            
            ?>
    <tr data-id="<?=$campos['id']?>">
            <td><?= $campos['nomeCondominio'] ?></td>
            <td><?= $campos['qtde'] ?></td>
            <td><?= $campos['logradouro'] ?></td>
            <td><?= $campos['numero'] ?></td>
            <td><?= $campos['bairro'] ?></td>
            <td><?= $campos['cidade'] ?></td>
            <td><?= $campos['uf'] ?></td>
            <td><?= $campos['cep'] ?></td>
            <td><?= $campos['nome'] ?></td>
            <td><?= $campos['nomeAdm'] ?></td>
            <td><?= dateFormat($campos['dataCadastro'])?></td>
            <td><?= dateFormat($campos['dataUpdate'])?></td>
            <td>
                <a href="<?=$url_site?>condominio/id/<?=$campos['id']?>">Editar</a>
                <a href="#" data-id="<?=$campos['id']?>" class="removerCondominio">Remover</a>
            </td>
    </tr>
<? } ?>
<tr>
    <td colspan="3"> &nbsp;</td>
    <td colspan="12" class="totalRegistros">Total de registros: <?=$totalRegistros?></td>
</tr>
</table>
<div class="class col-sm-12">
    <?=$paginacao?>
</div>

<?
    if(!empty($_GET['deletar'])){
        unset($_SESSION['cadastro'][$_GET['deletar']]);
        header("Location: index.php?page=listaCondominios");
    }
?>