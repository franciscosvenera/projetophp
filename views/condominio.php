
<form action="" method="post" id="cadastroCondominio">
    
    
    Nome do condomínio:
    <input class="col col-6 mt-2 form-control" type="text" name="nomeCondominio" value="<?=$condominio['nomeCondominio']?>" required>
    Quantidade de blocos:
    <input class="col col-6 mt-2 form-control" type="text" name="qtde" value="<?=$condominio['qtde']?>" required>
    Logradouro:
    <input class="col col-6 mt-2 form-control" type="text" name="logradouro" value="<?=$condominio['logradouro']?>" required>
    Número:
    <input class="col col-6 mt-2 form-control" type="text" name="numero" value="<?=$condominio['numero']?>">
    Bairro:
    <input class="col col-6 mt-2 form-control" type="text" name="bairro" value="<?=$condominio['bairro']?>">
    Cidade:
    <input class="col col-6 mt-2 form-control" type="text" name="cidade" value="<?=$condominio['cidade']?>">
    Estado:
    <select name="uf" class="form-control">
    <? foreach($estados as $uf=>$estado){?>
            <option value="<?=$uf?>"<?=($uf == $condominio['uf'] ? 'selected' : '')?>><?=$estado?></option>
            <?}?>
    </select>
    CEP:
    <input class="col col-6 mt-2 form-control" type="text" name="cep" value="<?=$condominio['cep']?>">
    Sindico:
    <select name="sindico" class="form-control">
        
        <?foreach ($resultConselho as $ch => $value) {?>
            <option value="<?=$value['id']?>"<?=($value['nome'] == $condominio['nome'] ? 'selected' : '')?>><?=$value['nome']?></option>
        <?}?>
    </select>
    Administradora:
    <select name="idAdmin" class="form-control">
        
        <?foreach ($resultAdministradora as $ch => $value) {?>
            <option value="<?=$value['id']?>"<?=($value['nomeAdm'] == $condominio['nomeAdm'] ? 'selected' : '')?>><?=$value['nomeAdm']?></option>
        <?}?>
    </select>

    <? if($_GET['id']){?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <?}?>
    <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>