<?
//require "uteis.php";
?>

<table class="col col-12 table table-striped mt-5" id="listaAdministradoras">
    <tr>
        <td>Nome</td>
        <td>CNPJ</td>
        <td><a href="<?=$url_site?>listaAdministradoras" class="btn btn-primary">Adicionar</a> </td>
    </tr>
    <? 
        foreach ($result['resultSet'] as $chDados => $campos) { 
            
    ?>
    <tr data-id="<?=$campos['id']?>">
            <td><?= $campos['nomeAdm'] ?></td>
            <td><?= $campos['CNPJ'] ?></td>
            
            <td>
                <a href="<?=$url_site?>administradora/id/<?=$campos['id']?>">Editar</a>
                <a href="#" data-id="<?=$campos['id']?>" class="removerAdministradora">Remover</a>
            </td>
    </tr>
<? } ?>
<tr>
    <td colspan="3"> &nbsp;</td>
    <td colspan="12" class="totalRegistros">Total de registros: <?=$totalRegistros ?></td>
</tr>
</table>

<?
    if(!empty($_GET['deletar'])){
        unset($_SESSION['cadastro'][$_GET['deletar']]);
        header("Location: index.php?page=listaAdministradoras");
    }
?>