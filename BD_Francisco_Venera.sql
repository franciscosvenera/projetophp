-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.22-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para fv_sistema
CREATE DATABASE IF NOT EXISTS `fv_sistema` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `fv_sistema`;

-- Copiando estrutura para tabela fv_sistema.fv_administradora
CREATE TABLE IF NOT EXISTS `fv_administradora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeAdm` varchar(50) DEFAULT NULL,
  `CNPJ` varchar(14) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_administradora: ~5 rows (aproximadamente)
DELETE FROM `fv_administradora`;
/*!40000 ALTER TABLE `fv_administradora` DISABLE KEYS */;
INSERT INTO `fv_administradora` (`id`, `nomeAdm`, `CNPJ`, `dataCadastro`) VALUES
	(1, 'Trend', '07715302000195', '2022-03-30 13:20:20'),
	(2, 'A Moradia', '06709561000140', '2022-03-30 13:21:00'),
	(3, 'Concetra', '26961303000164', '2022-03-30 13:21:36'),
	(5, 'administradora', NULL, NULL),
	(6, 'outra Admi', NULL, NULL),
	(7, 'mais uma', NULL, NULL),
	(8, 'Adm Teste', '000011111', NULL);
/*!40000 ALTER TABLE `fv_administradora` ENABLE KEYS */;

-- Copiando estrutura para tabela fv_sistema.fv_bloco
CREATE TABLE IF NOT EXISTS `fv_bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeBloco` varchar(255) NOT NULL DEFAULT '',
  `andares` varchar(11) NOT NULL DEFAULT '',
  `unidadesAndar` varchar(255) NOT NULL DEFAULT '',
  `idCondominio` int(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Bloco_Condominio` (`idCondominio`),
  CONSTRAINT `FK_Bloco_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_bloco: ~8 rows (aproximadamente)
DELETE FROM `fv_bloco`;
/*!40000 ALTER TABLE `fv_bloco` DISABLE KEYS */;
INSERT INTO `fv_bloco` (`id`, `nomeBloco`, `andares`, `unidadesAndar`, `idCondominio`) VALUES
	(1, 'A', '5', '4', 8),
	(2, 'B', '5', '4', 4),
	(3, 'Bloco', '4', '5', 2),
	(4, 'Bloco', '4', '5', 3),
	(5, 'Torre A', '10', '4', 1),
	(7, 'Torre A', '10', '5', 1),
	(8, 'Bloco b', '10', '5', 8),
	(9, 'Torre A', '10', '5', 8),
	(10, 'Bloco A', '10', '5', 3);
/*!40000 ALTER TABLE `fv_bloco` ENABLE KEYS */;

-- Copiando estrutura para tabela fv_sistema.fv_cadastro
CREATE TABLE IF NOT EXISTS `fv_cadastro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `fone` varchar(50) NOT NULL DEFAULT '',
  `idCondominio` int(11) NOT NULL DEFAULT 0,
  `idBloco` int(11) NOT NULL DEFAULT 0,
  `idUnidade` int(11) NOT NULL DEFAULT 0,
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Morador_Bloco` (`idBloco`),
  KEY `FK_Morador_Condominio` (`idCondominio`),
  KEY `FK_Morador_Unidade` (`idUnidade`),
  CONSTRAINT `FK_Morador_Bloco` FOREIGN KEY (`idBloco`) REFERENCES `fv_bloco` (`id`),
  CONSTRAINT `FK_Morador_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`),
  CONSTRAINT `FK_Morador_Unidade` FOREIGN KEY (`idUnidade`) REFERENCES `fv_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_cadastro: ~15 rows (aproximadamente)
DELETE FROM `fv_cadastro`;
/*!40000 ALTER TABLE `fv_cadastro` DISABLE KEYS */;
INSERT INTO `fv_cadastro` (`id`, `nome`, `cpf`, `email`, `fone`, `idCondominio`, `idBloco`, `idUnidade`, `dataUpdate`, `dataCadastro`) VALUES
	(2, 'João dos Santos', '98765432101', 'joao@gmail.com', '99999999', 1, 1, 2, '2022-03-30 16:51:15', '2022-03-30 16:51:15'),
	(3, 'Maria Joaquina', '11111111111', 'maria@gmail.com', '9999999999', 1, 1, 1, '2022-03-29 14:55:34', '2022-03-29 14:55:34'),
	(6, 'Mario Silveira', '', '', '', 6, 2, 5, '2022-03-30 16:58:38', '2022-03-30 16:58:38'),
	(7, 'Ana Beatriz', '', '', '', 4, 3, 4, '2022-03-30 16:59:18', '2022-03-30 16:59:18'),
	(9, 'Luiza Rita Cardoso', '59776764856', 'luiza@rita.com', '', 2, 1, 2, '2022-03-31 08:54:55', '2022-03-31 08:54:55'),
	(10, 'Analu Mariah Sophia Fogaça', '21331830056', 'analu@gmail.com', '', 5, 3, 2, '2022-03-31 08:55:01', '2022-03-31 08:55:01'),
	(12, 'Stefany Bárbara Clara da Silva', '73163246356', 'sb@gmail.com', '', 8, 1, 1, '2022-03-31 08:55:17', '2022-03-31 08:55:17'),
	(13, 'Guilherme Juan Samuel Dias', '02618127956', 'guilherme@gmail.com', '', 8, 4, 5, '2022-03-31 08:55:24', '2022-03-31 08:55:24'),
	(15, 'Gabriela Lúcia Nogueira', '46674573856', '', '', 4, 1, 1, '2022-03-31 08:55:30', '2022-03-31 08:55:30'),
	(20, 'ana', '1111', 'a@a', '2222', 1, 1, 1, '2022-04-01 09:11:34', '2022-04-01 09:11:34'),
	(21, 'José da Silva', '053.387.819', 'a@aaaa', '4444', 1, 1, 1, '2022-04-01 10:28:30', '2022-04-01 10:28:30'),
	(25, 'José da Silva', '1111', 'a@a', '2222', 8, 1, 1, '2022-04-01 13:40:31', '2022-04-01 13:40:31'),
	(26, 'José da Silva', '1112335', 'a@aaaa', '4444', 3, 3, 2, '2022-04-01 13:50:36', '2022-04-01 13:50:36'),
	(27, 'a', '1112335', 'a@a', '2222', 8, 1, 1, '2022-04-06 14:39:43', '2022-04-06 14:39:43');
/*!40000 ALTER TABLE `fv_cadastro` ENABLE KEYS */;

-- Copiando estrutura para tabela fv_sistema.fv_condominios
CREATE TABLE IF NOT EXISTS `fv_condominios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCondominio` varchar(255) NOT NULL DEFAULT '',
  `qtde` int(11) NOT NULL DEFAULT 0,
  `logradouro` varchar(50) NOT NULL DEFAULT '',
  `numero` varchar(50) NOT NULL DEFAULT '',
  `bairro` varchar(50) NOT NULL DEFAULT '',
  `cidade` varchar(50) NOT NULL DEFAULT '',
  `uf` varchar(50) NOT NULL DEFAULT '',
  `cep` varchar(8) NOT NULL DEFAULT '',
  `sindico` int(11) NOT NULL DEFAULT 0,
  `idAdmin` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Condominio_Sindico` (`sindico`),
  KEY `FK_Condominio_Admin` (`idAdmin`),
  CONSTRAINT `FK_Condominio_Admin` FOREIGN KEY (`idAdmin`) REFERENCES `fv_administradora` (`id`),
  CONSTRAINT `FK_Condominio_Sindico` FOREIGN KEY (`sindico`) REFERENCES `fv_conselho` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_condominios: ~7 rows (aproximadamente)
DELETE FROM `fv_condominios`;
/*!40000 ALTER TABLE `fv_condominios` DISABLE KEYS */;
INSERT INTO `fv_condominios` (`id`, `nomeCondominio`, `qtde`, `logradouro`, `numero`, `bairro`, `cidade`, `uf`, `cep`, `sindico`, `idAdmin`) VALUES
	(1, 'Condomínio das Palmeiras', 4, 'Rua Maria Simão', '1000', 'Centro', 'Indaial', 'PI', '89130000', 1, 1),
	(2, 'Condomínio das flores', 8, 'Rua Manoel Simão', '500', 'Centro', 'Blumenau', 'SC', '89000000', 2, 1),
	(3, 'Condominio Jardim da Saudade', 6, 'Rua das Nações', '200', 'Centro', 'Indaial', 'AC', '89130000', 1, 1),
	(4, 'Condominio Dallas', 10, 'Avenida Brasil', '100', 'Nações', 'Timbó', 'SC', '8901000', 2, 3),
	(5, 'Condominio Palermo', 6, 'Rua Minas Gerais', '50', 'Tapajós', 'Indaial', 'SC', '89130000', 2, 3),
	(6, 'Condominio Morada das estrelas', 4, 'Rua Emilio Odebrecth', '120', 'Centro', 'Indaia', 'AC', '89130000', 1, 1),
	(8, 'Condominio AP Controle', 8, 'Rua Rio do Sul', '200', 'Rio Morto', 'Indaial', 'SC', '89130000', 1, 2);
/*!40000 ALTER TABLE `fv_condominios` ENABLE KEYS */;

-- Copiando estrutura para tabela fv_sistema.fv_conselho
CREATE TABLE IF NOT EXISTS `fv_conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `fone` varchar(50) NOT NULL DEFAULT '',
  `funcao` enum('Subsindico','Conselho fiscal') NOT NULL,
  `idCondominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Conselho_Condominio` (`idCondominio`) USING BTREE,
  CONSTRAINT `FK_Conselho_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_conselho: ~16 rows (aproximadamente)
DELETE FROM `fv_conselho`;
/*!40000 ALTER TABLE `fv_conselho` DISABLE KEYS */;
INSERT INTO `fv_conselho` (`id`, `nome`, `cpf`, `email`, `fone`, `funcao`, `idCondominio`) VALUES
	(1, 'Francisco Venera', '00000000000', 'francisco@gmail.com', '33333333', 'Subsindico', 1),
	(2, 'Tarcisio', '00000000001', 'tarcisio@gmail.com', '99999999', 'Conselho fiscal', 1),
	(3, 'José da SIlva', '', '', '', 'Subsindico', 2),
	(4, 'Sebastião Moreira', '', '', '', 'Conselho fiscal', 2),
	(5, 'Maria dos Santos', '', '', '', 'Subsindico', 3),
	(6, 'Bastiana Pereira', '', '', '', 'Conselho fiscal', 3),
	(7, 'Ana Carolina', '', '', '', 'Subsindico', 4),
	(8, 'Tereza Cristina', '', '', '', 'Conselho fiscal', 4),
	(9, 'Josiane Santos', '', '', '', 'Conselho fiscal', 5),
	(10, 'William Rodeio', '', '', '', 'Subsindico', 5),
	(11, 'Diego', '', '', '', 'Subsindico', 6),
	(12, 'Maicon', '', '', '', 'Conselho fiscal', 6),
	(13, 'Marcia', '', '', '', 'Subsindico', 8),
	(14, 'Mario', '', '', '', 'Conselho fiscal', 8),
	(15, 'Jeferson', '', '', '', 'Conselho fiscal', 8),
	(16, 'Juliana', '', '', '', 'Conselho fiscal', 8);
/*!40000 ALTER TABLE `fv_conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela fv_sistema.fv_pets
CREATE TABLE IF NOT EXISTS `fv_pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomePet` varchar(50) DEFAULT NULL,
  `tipo` enum('Cachorro','Gato') DEFAULT NULL,
  `id_morador` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PET_MORADOR` (`id_morador`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fv_sistema.fv_pets: ~3 rows (aproximadamente)
DELETE FROM `fv_pets`;
/*!40000 ALTER TABLE `fv_pets` DISABLE KEYS */;
INSERT INTO `fv_pets` (`id`, `nomePet`, `tipo`, `id_morador`, `dataCadastro`) VALUES
	(1, 'Pitty', 'Cachorro', 2, '2022-03-30 11:31:39'),
	(2, 'Meg', 'Gato', 3, '2022-03-30 11:31:38'),
	(3, 'Dimmy', 'Cachorro', 3, '2022-03-30 11:32:07');
/*!40000 ALTER TABLE `fv_pets` ENABLE KEYS */;

-- Copiando estrutura para tabela fv_sistema.fv_reserva_salao_festa
CREATE TABLE IF NOT EXISTS `fv_reserva_salao_festa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tituloEvento` varchar(50) DEFAULT '',
  `idUnidade` int(11) DEFAULT NULL,
  `dataHoraEvento` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `dataCadastro` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fv_reserva_salao_festa_fv_unidade` (`idUnidade`),
  CONSTRAINT `FK_fv_reserva_salao_festa_fv_unidade` FOREIGN KEY (`idUnidade`) REFERENCES `fv_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fv_sistema.fv_reserva_salao_festa: ~2 rows (aproximadamente)
DELETE FROM `fv_reserva_salao_festa`;
/*!40000 ALTER TABLE `fv_reserva_salao_festa` DISABLE KEYS */;
INSERT INTO `fv_reserva_salao_festa` (`id`, `tituloEvento`, `idUnidade`, `dataHoraEvento`, `dataCadastro`) VALUES
	(2, 'Festa dos formandos', 2, '2022-04-10 21:00:00', NULL),
	(3, 'Festa de aniversário', 5, '2022-04-15 19:00:00', NULL),
	(4, 'Festa do condomínio', 6, '2022-04-05 20:00:00', NULL);
/*!40000 ALTER TABLE `fv_reserva_salao_festa` ENABLE KEYS */;

-- Copiando estrutura para tabela fv_sistema.fv_unidade
CREATE TABLE IF NOT EXISTS `fv_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroUnidade` int(50) NOT NULL DEFAULT 0,
  `metragem` float NOT NULL DEFAULT 0,
  `vagasGaragem` int(50) NOT NULL DEFAULT 0,
  `idBloco` int(50) NOT NULL,
  `idCondominio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_Unidade_Bloco` (`idBloco`),
  KEY `FK_Unidade_Condominio` (`idCondominio`),
  CONSTRAINT `FK_Unidade_Bloco` FOREIGN KEY (`idBloco`) REFERENCES `fv_bloco` (`id`),
  CONSTRAINT `FK_Unidade_Condominio` FOREIGN KEY (`idCondominio`) REFERENCES `fv_condominios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela fv_sistema.fv_unidade: ~9 rows (aproximadamente)
DELETE FROM `fv_unidade`;
/*!40000 ALTER TABLE `fv_unidade` DISABLE KEYS */;
INSERT INTO `fv_unidade` (`id`, `numeroUnidade`, `metragem`, `vagasGaragem`, `idBloco`, `idCondominio`) VALUES
	(1, 101, 55, 2, 1, 1),
	(2, 102, 55, 2, 1, 2),
	(3, 103, 55, 2, 1, 1),
	(4, 104, 55, 1, 1, 2),
	(5, 201, 100, 1, 2, 1),
	(6, 202, 100, 1, 2, 2),
	(8, 101, 56, 1, 3, 3),
	(9, 102, 56, 1, 1, 4),
	(10, 101, 56, 1, 0, 1);
/*!40000 ALTER TABLE `fv_unidade` ENABLE KEYS */;

-- Copiando estrutura para tabela fv_sistema.fv_usuarios
CREATE TABLE IF NOT EXISTS `fv_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `usuario` varchar(50) NOT NULL DEFAULT '',
  `senha` varchar(50) NOT NULL DEFAULT '',
  `datacadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fv_sistema.fv_usuarios: ~1 rows (aproximadamente)
DELETE FROM `fv_usuarios`;
/*!40000 ALTER TABLE `fv_usuarios` DISABLE KEYS */;
INSERT INTO `fv_usuarios` (`id`, `nome`, `usuario`, `senha`, `datacadastro`) VALUES
	(1, 'Francisco Venera', 'francisco', '1234', '2022-04-05 15:47:29'),
	(14, 'José da Silva', 'francisco', '1', '2022-04-06 16:49:48');
/*!40000 ALTER TABLE `fv_usuarios` ENABLE KEYS */;

-- Copiando estrutura para tabela fv_sistema.lfv_lista_convidados
CREATE TABLE IF NOT EXISTS `lfv_lista_convidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `convidado` varchar(50) NOT NULL DEFAULT '0',
  `cpf` varchar(11) NOT NULL DEFAULT '0',
  `celular` varchar(10) NOT NULL DEFAULT '0',
  `idReservaSalao` int(11) NOT NULL DEFAULT 0,
  `idUnidade` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_convidados_salao` (`idReservaSalao`),
  KEY `FK_convidados_unidade` (`idUnidade`),
  CONSTRAINT `FK_convidados_salao` FOREIGN KEY (`idReservaSalao`) REFERENCES `fv_reserva_salao_festa` (`id`),
  CONSTRAINT `FK_convidados_unidade` FOREIGN KEY (`idUnidade`) REFERENCES `fv_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela fv_sistema.lfv_lista_convidados: ~14 rows (aproximadamente)
DELETE FROM `lfv_lista_convidados`;
/*!40000 ALTER TABLE `lfv_lista_convidados` DISABLE KEYS */;
INSERT INTO `lfv_lista_convidados` (`id`, `convidado`, `cpf`, `celular`, `idReservaSalao`, `idUnidade`) VALUES
	(1, 'Calebe Nelson Peixoto', '278.478.188', '0', 2, 2),
	(3, 'Heloise Pietra Gonçalves', '116.279.967', '0', 2, 2),
	(4, 'Thiago Benedito Drumond', '457.671.981', '0', 2, 1),
	(5, 'Victor Lucca Dias', '038.158.430', '0', 2, 1),
	(6, 'Adriana Giovanna Lívia Ferreira', '715.759.937', '0', 2, 3),
	(7, 'Eliane Maria Clara Assunção', '434.975.033', '0', 2, 3),
	(8, 'Thales Carlos Eduardo Ruan da Paz', '239.077.759', '0', 3, 4),
	(9, 'Henry Leandro Caldeira', '557.657.863', '0', 3, 4),
	(10, 'Anthony Bruno Márcio Moreira', '172.715.094', '0', 3, 5),
	(11, 'Jorge Isaac Aragão', '185.270.915', '0', 3, 5),
	(12, 'Bento Bruno Diego de Paula', '134.417.750', '0', 4, 6),
	(13, 'Pedro Iago Nathan Pinto', '825.349.579', '0', 4, 6),
	(15, 'Vitor Enrico Farias', '152.001.022', '0', 4, 6),
	(16, 'Marcela Luiza Silva', '369.029.323', '0', 4, 4);
/*!40000 ALTER TABLE `lfv_lista_convidados` ENABLE KEYS */;

-- Copiando estrutura para view fv_sistema.vw_consulta_bloco
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_consulta_bloco` (
	`id` INT(11) NOT NULL,
	`nomeBloco` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`andares` VARCHAR(11) NOT NULL COLLATE 'utf8mb4_general_ci',
	`unidadesAndar` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`nomeCondominio` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`idCondominio` INT(50) NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_consulta_cadastro
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_consulta_cadastro` (
	`id` INT(11) NOT NULL,
	`nome` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`cpf` VARCHAR(11) NOT NULL COLLATE 'utf8mb4_general_ci',
	`email` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`fone` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`dataCadastro` TIMESTAMP NOT NULL,
	`nomeCondominio` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`nomeBloco` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`numeroUnidade` INT(50) NULL,
	`idCondominio` INT(11) NOT NULL,
	`idBloco` INT(11) NOT NULL,
	`idUnidade` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_consulta_condominio
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_consulta_condominio` (
	`id` INT(11) NOT NULL,
	`nomeCondominio` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`qtde` INT(11) NOT NULL,
	`logradouro` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`numero` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`bairro` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`cidade` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`uf` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_general_ci',
	`cep` VARCHAR(8) NOT NULL COLLATE 'utf8mb4_general_ci',
	`nome` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`nomeAdm` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`sindico` INT(11) NOT NULL,
	`idAdmin` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_consulta_unidade
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_consulta_unidade` (
	`id` INT(11) NOT NULL,
	`numeroUnidade` INT(50) NOT NULL,
	`metragem` FLOAT NOT NULL,
	`vagasGaragem` INT(50) NOT NULL,
	`nomeBloco` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`nomeCondominio` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`idBloco` INT(50) NOT NULL,
	`idCondominio` INT(11) NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_convidados_festa
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_convidados_festa` (
	`id` INT(11) NOT NULL,
	`tituloEvento` VARCHAR(50) NULL COLLATE 'utf8mb4_general_ci',
	`idUnidade` INT(11) NULL,
	`dataHoraEvento` TIMESTAMP NULL,
	`numeroUnidade` INT(50) NOT NULL,
	`nome` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`Name_exp_7` MEDIUMTEXT NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_moradores_por_condominio
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_moradores_por_condominio` (
	`nomeCondominio` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`Total` BIGINT(21) NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view fv_sistema.vw_consulta_bloco
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_consulta_bloco`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_consulta_bloco` AS SELECT 
bl.id, bl.nomeBloco, bl.andares, bl.unidadesAndar, cond.nomeCondominio, bl.idCondominio
FROM
fv_bloco bl 
INNER JOIN fv_condominios cond ON bl.idCondominio = cond.id
ORDER BY cond.nomeCondominio ;

-- Copiando estrutura para view fv_sistema.vw_consulta_cadastro
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_consulta_cadastro`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_consulta_cadastro` AS SELECT
cad.id, cad.nome, cad.cpf, cad.email, cad.fone, cad.dataCadastro, cond.nomeCondominio, bl.nomeBloco, uni.numeroUnidade, cad.idCondominio, cad.idBloco, cad.idUnidade 
FROM 
fv_cadastro cad
LEFT JOIN fv_unidade uni ON cad.idUnidade = uni.id
LEFT JOIN fv_bloco bl ON cad.idBloco = bl.id
LEFT JOIN fv_condominios cond ON cad.idCondominio = cond.id ;

-- Copiando estrutura para view fv_sistema.vw_consulta_condominio
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_consulta_condominio`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_consulta_condominio` AS SELECT
cond.id, cond.nomeCondominio, cond.qtde, cond.logradouro, cond.numero, cond.bairro, cond.cidade, cond.uf, cond.cep, con.nome, adm.nomeAdm, cond.sindico, cond.idAdmin
FROM
fv_condominios cond
LEFT JOIN fv_conselho con ON cond.sindico = con.id
LEFT JOIN fv_administradora adm ON cond.idAdmin = adm.id ;

-- Copiando estrutura para view fv_sistema.vw_consulta_unidade
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_consulta_unidade`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_consulta_unidade` AS SELECT 
uni.id, 
uni.numeroUnidade, 
uni.metragem,
uni.vagasGaragem, 
bl.nomeBloco, 
cond.nomeCondominio, 
uni.idBloco, 
uni.idCondominio
FROM
fv_unidade uni
LEFT JOIN fv_bloco bl ON uni.idBloco = bl.id
LEFT JOIN fv_condominios cond ON uni.idCondominio = cond.id ;

-- Copiando estrutura para view fv_sistema.vw_convidados_festa
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_convidados_festa`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_convidados_festa` AS SELECT festa.id, festa.tituloEvento, festa.idUnidade, festa.dataHoraEvento, un.numeroUnidade, cad.nome,
 
(SELECT GROUP_CONCAT(' ',lista.convidado)  FROM lfv_lista_convidados lista WHERE idReservaSalao = festa.id)
FROM fv_reserva_salao_festa festa
INNER JOIN fv_unidade un ON festa.idUnidade = un.id
LEFT JOIN fv_cadastro cad ON cad.idUnidade = un.id ;

-- Copiando estrutura para view fv_sistema.vw_moradores_por_condominio
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_moradores_por_condominio`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_moradores_por_condominio` AS SELECT
cond.nomeCondominio,
COUNT(cad.id) AS Total
FROM 
fv_cadastro cad
LEFT JOIN fv_condominios cond ON cad.idCondominio = cond.id
GROUP BY cad.idCondominio ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
