<?

use function PHPSTORM_META\type;

include "uteis.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Ap Controle - Login</title>
</head>
<body>
    <main class="container">
        <form action="<?=$url_site?>/controllers/restrito.php" method="POST">
            <div class="form-group">
            <div class="col-sm-6 form-group mt-5">
                    <img src="img/logo.png" alt="logo.png">
                    <h4 for="login">Efetue seu login</h4>
                </div>
                <div class="col-sm-6 form-group">
                    <label for="login">Login</label>
                    <input class="form-control" type="text" name="usuario" required>
                </div>

                <div class="col-sm-6 form-group">
                    <label for="senha">Senha</label>
                    <input class="form-control" type="password" name="senha" required>
                </div>
                <div class="col-sm-6 form-group">
                    <br>
                    <button class="btn btn-primary">Entrar</button>

                </div>
            </div>
        </form>
    </main>
    
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/app.js?v=<?=rand(0,9999)?>"></script>
    <? if(isset($_GET['msg'])){?>
    <script type="text/javascript">
        $(function(){
            myAlert('danger','<?=$_GET['msg']?>','main');
        })
    </script>
    <? } ?>
</body>
</html>